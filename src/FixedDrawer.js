import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Drawer from '@material-ui/core/Drawer';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';

class FixedDrawer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            list: []
        }
    }

    /**
     * Change field by name
     * @param name {String}
     */
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    /**
     * Change item of array by index
     * @param index {Number}
     */
    handleChangeList = index => event => {
        const newList = this.props.dataActive.list.slice();
        newList[index][event.target.name] = event.target.value;
        this.setState({
            list: newList
        })
    };

    /**
     * If the active element has changed, then we change value of state
     * @param prevProps {Object}
     */
    componentDidUpdate(prevProps) {
        if(this.props.dataActive !== prevProps.dataActive) {
            this.setState({
                name: this.props.dataActive.name,
                list: this.props.dataActive.list
            })
        }
    }

    render() {
        let {open, classes, dataActive, handleSave} = this.props;
        let {name, list} = this.state;

        return (
            <Drawer
                variant="persistent"
                anchor='right'
                open={open}
                classes={{paper: classes.drawerPaper}}>
                {dataActive && <form style={{padding: '20px'}}>
                    <h2>You are editing element with name: {dataActive.name}</h2>
                    <TextField
                        id="name"
                        label="Name"
                        className={classes.textfield}
                        value={name || dataActive.name}
                        onChange={this.handleChange('name')}
                        margin="normal"
                    />
                    {
                        dataActive.list.map((listItem, index) => (
                            <div style={{display: 'flex'}} key={'listField' + index}>
                                <TextField
                                    label="Primary"
                                    name="primary"
                                    className={classes.textfield}
                                    value={list[index] ? list[index].primary : dataActive.list[index].primary}
                                    onChange={this.handleChangeList(index)}
                                    margin="normal"
                                />
                                <TextField
                                    label="Secondary"
                                    name="secondary"
                                    className={classes.textfield}
                                    value={list[index] ?  list[index].secondary : dataActive.list[index].secondary}
                                    onChange={this.handleChangeList(index)}
                                    margin="normal"
                                />
                            </div>
                        ))
                    }
                    <Button variant="contained" color="primary" className={classes.button} onClick={handleSave.bind(this, name, list)}>
                        Save
                    </Button>
                </form>}
            </Drawer>
        );
    }
}


FixedDrawer.propTypes = {
    dataActive: PropTypes.array,
    open: PropTypes.bool.isRequired,
    handleSave: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired
};

export default FixedDrawer;
