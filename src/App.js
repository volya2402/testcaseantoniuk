import React, {Component} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Create from '@material-ui/icons/Create';
import MyLocation from '@material-ui/icons/MyLocation';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import classNames from 'classnames';
import FixedDrawer from './FixedDrawer';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';

const drawerWidth = 400;

const styles = theme => ({
    root: {
        flexGrow: 1,
        mawWidth: 1400,
        margin: '0 auto'
    },
    appFrame: {
        minHeight: '100vh',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    'appBarShift-right': {
        marginRight: drawerWidth,
    },
    button: {
        display: 'block',
        maxWidth: 150,
        margin: '10px auto'
    },
    drawerPaper: {
        position: 'fixed',
        width: drawerWidth,
    },
    card: {
        margin: '20px 0'
    },
    'content-right': {
        marginRight: drawerWidth,
    },
    'contentShift-right': {
        marginRight: 0,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    textfield: {
        width: '100%'
    }
});

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openDrawer: false,
            indexActiveItem: null,
            data: [
                {
                    name: 'Test',
                    list: [
                        {
                            primary: "Photos",
                            secondary: "Jan 19, 2014"
                        },
                        {
                            primary: "Text",
                            secondary: "Jan 17, 2014"
                        }
                    ]
                },
                {
                    name: 'Data',
                    list: [
                        {
                            primary: "Folder",
                            secondary: "Jan 22, 2014"
                        },
                        {
                            primary: "Test",
                            secondary: "Jan 12, 2014"
                        }
                    ]
                },
                {
                    name: 'TestTestTest',
                    list: [
                        {
                            primary: "Photos",
                            secondary: "Jan 19, 2014"
                        }
                    ]
                },
                {
                    name: 'DataData',
                    list: [
                        {
                            primary: "Data",
                            secondary: "Jan 22, 2014"
                        }
                    ]
                }
            ]
        }
    }

    /**
     * @function Open/Close drawer and change active item
     * @param index {Number} - index target item in Array
     */
    toggleDrawer = index => {
        if(this.state.indexActiveItem === index) {
            this.setState({openDrawer: !this.state.openDrawer, indexActiveItem: index});
        } else {
            this.setState({openDrawer: true, indexActiveItem: index});
        }
    };

    /**
     * @function Update data
     * @param newName {String} - new name of item
     * @param newList {Array} - updated info
     */
    handleSave = (newName, newList) => {
        let {data, indexActiveItem} = this.state;
        let newData = [...data];
        newData[indexActiveItem].name = newName;
        newData[indexActiveItem].list = newList;
        this.setState({
            data: newData
        })
    };

    render() {
        const {classes} = this.props;
        const {openDrawer, data, indexActiveItem} = this.state;

        return (
            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <main
                        className={classNames(classes.content, classes[`content-right`], {
                            [classes.contentShift]: !openDrawer,
                            [classes[`contentShift-right`]]: !openDrawer,
                        })}>
                        <Grid container>
                            <Grid item xs>
                                {
                                    data.map((item, index) => (
                                        <Card key={item.name + index} className={classes.card}>
                                            <CardHeader
                                                avatar={<MyLocation />}
                                                title={item.name}
                                                action={
                                                    <IconButton onClick={this.toggleDrawer.bind(this, index)}>
                                                        <Create />
                                                    </IconButton>
                                                }
                                            />
                                            <CardContent>
                                                <List>
                                                {
                                                    item.list.map((listItem, index) => (
                                                            <ListItem key={'list' + item.name + index}>
                                                                <Avatar>
                                                                    <Create />
                                                                </Avatar>
                                                                <ListItemText primary={listItem.primary} secondary={listItem.secondary}/>
                                                            </ListItem>
                                                    ))
                                                }
                                                </List>
                                            </CardContent>
                                        </Card>
                                    ))
                                }
                            </Grid>
                        </Grid>
                    </main>
                    <FixedDrawer open={openDrawer} classes={classes} dataActive={data[indexActiveItem]} handleSave={this.handleSave}/>
                </div>
            </div>
        );
    }
}

App.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
